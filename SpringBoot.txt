{org.hibernate.flushMode=AUTO}

EntityManagerFactoryBuilderImpl

	@SuppressWarnings("unchecked")
	public EntityManagerFactory build() {
		SessionFactoryBuilder sfBuilder = metadata().getSessionFactoryBuilder();
		populate( sfBuilder, standardServiceRegistry );

		SessionFactoryImplementor sessionFactory;
		try {
			sessionFactory = (SessionFactoryImplementor) sfBuilder.build();
		}
		catch (Exception e) {
			throw persistenceException( "Unable to build Hibernate SessionFactory", e );
		}

		JpaSchemaGenerator.performGeneration( metadata(), configurationValues, standardServiceRegistry );

		return new EntityManagerFactoryImpl(
				persistenceUnit.getName(),
				sessionFactory,
				metadata(),
				settings,
				configurationValues
		);
	}
	
	
	EntityManagerFactoryImpl

	
	 properties.put("javax.persistence.transactionType", "JTA");
	 
	 
	 {hibernate.implicit_naming_strategy=org.springframework.boot.orm.jpa.hibernate.SpringImplicitNamingStrategy, hibernate.hbm2ddl.auto=update, hibernate.id.new_generator_mappings=false, hibernate.current_session_context_class=thread, hibernate.physical_naming_strategy=org.springframework.boot.orm.jpa.hibernate.SpringPhysicalNamingStrategy, hibernate.transaction.factory_class=org.hibernate.transaction.CMTTransactionFactory, hibernate.transaction.jta.platform=org.hibernate.engine.transaction.jta.platform.internal.NoJtaPlatform@35b17c06}