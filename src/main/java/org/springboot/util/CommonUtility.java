package org.springboot.util;

import org.springframework.stereotype.Component;

@Component
public class CommonUtility {

	public CommonUtility(){
		System.out.println("Inside CommonUtility ");
		System.out.println("Value of SystemProperty is "+System.getProperty("mykey"));
	}
}
