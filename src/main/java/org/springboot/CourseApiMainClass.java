package org.springboot;

import java.util.Arrays;
import java.util.HashSet;

import org.springboot.config.MyListener;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories(basePackages = "org.springboot")
public class CourseApiMainClass {

	public static void main(String[] args) {
		
		new SpringApplicationBuilder()
		//.listeners(new MyListener(),new MyListener())
		.sources(CourseApiMainClass.class)
		.run(args);
		//SpringApplication sa = new SpringApplication();
		//sa.addListeners(new MyListener());
		//sa.setSources(new HashSet<>(Arrays.asList(CourseApiMainClass.class)));
		//sa.run(args);
		

	}

}
