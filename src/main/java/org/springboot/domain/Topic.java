package org.springboot.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Transient;

@Entity
public class Topic {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="TOPIC_ID")
	private Long id;
	
	@Column(name="TOPIC_NAME")
	private String name;
	
	@Column(name="TOPIC_DESC")
	private String desc;

	@Transient
	private StatusEnum statusEnum;
	
	Topic(){
		
	}
	public Topic(Long id, String name, String desc) {
		super();
		this.id = id;
		this.name = name;
		this.desc = desc;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}
	public StatusEnum getStatusEnum() {
		return statusEnum;
	}
	public void setStatusEnum(StatusEnum statusEnum) {
		this.statusEnum = statusEnum;
	}
	
	@Override
	public String toString() {
		return "Topic [id=" + id + ", name=" + name + ", desc=" + desc + ", statusEnum=" + statusEnum + "]";
	}
	
	
}
