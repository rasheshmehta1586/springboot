package org.springboot.config;

import org.springframework.boot.context.event.ApplicationEnvironmentPreparedEvent;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;


public class MyListener implements ApplicationListener<ApplicationEvent> {

//	@Override
//	public void onApplicationEvent(ApplicationEnvironmentPreparedEvent event) {
//		System.out.println("Inside MyListener");
//		System.setProperty("mykey", "myvalue123");
//		 
//	}

	@Override
	public void onApplicationEvent(ApplicationEvent event) {
		System.out.println("event: " + event);
		System.setProperty("mykey", "myvalue123");
		
	}
  
   
}