package org.springboot.config;

import java.lang.reflect.Type;

import org.springboot.domain.StatusEnum;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

class StatusEnumDeserializer implements JsonDeserializer<StatusEnum>
{
  @Override
  public StatusEnum deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
      throws JsonParseException
  {
	  System.out.println("Customized StatusEnumDeserializer called");  
    StatusEnum[] scopes = StatusEnum.values();
    for (StatusEnum scope : scopes)
    {
      if (scope.name().equals(json.getAsString()))
        return scope;
    }
    System.out.println("value not present in enum");
    //throw new RuntimeException("value not present in enum");
    return StatusEnum.NOT_SUPPORTED;
  }
}