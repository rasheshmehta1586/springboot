package org.springboot.controllers;

import java.util.List;

import org.springboot.domain.Topic;
import org.springboot.services.TopicService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TopicController {

	@Autowired
	private TopicService service;
	
	@RequestMapping("/")
	public String hello(){
	
		return "hello";
	}
	
	@RequestMapping(value={"/topics"})
	public List<Topic> getAllTopics(){
		return service.getTopics();
	}
	
	@RequestMapping(value={"/topics"},method={RequestMethod.POST})
	public void addTopic(@RequestBody Topic topic, BindingResult result){
		
		System.out.println(topic);
		System.out.println(result);
		//service.addTopic(topic);
	}
}
