package org.springboot.services;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.transaction.Transactional;

import org.springboot.dao.TopicRepository;
import org.springboot.domain.Topic;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

@Service
public class TopicService {

	@Autowired
	private TopicRepository topicRepository;
	
//	private List<Topic> topics = new ArrayList<Topic>(
//			Arrays.asList(new Topic("java", "java","java desc"),
//						  new Topic("javascript","javascript","javascript")));
	
	@Transactional
	public List<Topic> getTopics(){
		
		System.out.println(TransactionAspectSupport.currentTransactionStatus());
		List<Topic> topicList = new ArrayList<Topic>();
		topicRepository.findAll().forEach(topic -> topicList.add(topic));
		return topicList;
	}
	
	
	@Transactional
	public void addTopic(Topic topic){
		topicRepository.save(topic);
	}
	
}
